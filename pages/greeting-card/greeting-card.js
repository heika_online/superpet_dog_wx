import { Common } from "../../utils/common.js";
const device = wx.getSystemInfoSync()
const width = device.windowWidth
const height = device.windowHeight - 43
const ctx = wx.createCanvasContext('petCard')
const miniCodeImg = "/style/images/mini_code.jpg"

Page({

  data: {
    width,
    height,
    petImg: "",
    petName: "",
    nickName: "",
    blessing: "",
    cardBg: "",
    yearStr: "祝您在2018",
    hasSave: false
  },

  onLoad: function (options) {
    let that = this
    console.log(options)
    if(options){
      that.setData({
        petImg: options.petImg,
        petName: options.petName,
        nickName: options.nickName,
        blessing: options.blessing,
        cardBg: options.cardBg
      })
    }
  },

  saveImage: function(){
    let that = this
    wx.showNavigationBarLoading()
    if (!that.data.hasSave){
      that.data.hasSave = true
      wx.canvasToTempFilePath({
        canvasId: 'petCard',
        success: function (res) {
          console.log(res.tempFilePath)
          wx.saveImageToPhotosAlbum({
            filePath: res.tempFilePath,
            success(res) {
              Common.myToast("已保存到手机相册", 2000);
            },
            fail: function(res){
              that.data.hasSave = false
            }
          })
        },
        fail: function(res){
          that.data.hasSave = false
        }
      })
    }else{
      Common.myToast("已保存到手机相册", 2000);
    }
    wx.hideNavigationBarLoading()
  },
  onReady: function(){
    let that = this
    //bg's size
    let sWidth = 640
    let sHeight = 960
    let sLineHeight = 320 // img fill height point
    let sTextLineHeight = 520 //text fill height point
    // let sMiniCodeHeight = 870 //mini code height point
    //fit the width, cal the new height
    let newHeight = width / sWidth * sHeight //new bg height
    let newLineHeight = newHeight / sHeight * sLineHeight // new img fill height
    // let newMiniCodeHeight = newHeight / sHeight * sMiniCodeHeight // new mini code fill height

    //img fill size
    let picHeight = 160 
    let picWidth = 160

    console.log("that.data.petImg==", that.data.petImg)
    //draw img
    ctx.drawImage(that.data.petImg, width / 2 - picWidth / 2, newLineHeight - picHeight/2, picWidth, picHeight)
    //draw bg
    ctx.drawImage(that.data.cardBg, 0, 0, 640, 960, 0, 0, width, newHeight)

    //new text fill height
    let newTextLineHeight = newHeight / sHeight * sTextLineHeight
    // text line height
    let textHeight = 35

    //fill first line text
    let firstLineText = that.data.nickName
    ctx.setFontSize(18)
    // ctx.setFillStyle("#ff8b5f")
    ctx.setFillStyle("#ffffff")
    ctx.setTextAlign('center')
    ctx.setTextBaseline('top')
    ctx.fillText(firstLineText, width/2, newTextLineHeight)
    newTextLineHeight += textHeight

    //fill second line text
    let secondLineText = "携"+that.data.petName
    ctx.setFontSize(18)
    ctx.setFillStyle("#ffffff")
    ctx.setTextAlign('center')
    ctx.setTextBaseline('top')
    ctx.fillText(secondLineText, width / 2, newTextLineHeight)
    newTextLineHeight += textHeight

    //fill third line text
    let thirdLineText = that.data.yearStr
    ctx.setFontSize(18)
    ctx.setFillStyle("#ffffff")
    ctx.setTextAlign('center')
    ctx.setTextBaseline('top')
    ctx.fillText(thirdLineText, width / 2, newTextLineHeight)
    newTextLineHeight += textHeight

    //fill forth line text
    let forthLineText = that.data.blessing
    ctx.setFontSize(20)
    ctx.setFillStyle("#ffffff")
    ctx.setTextAlign('center')
    ctx.setTextBaseline('top')
    ctx.fillText(forthLineText, width / 2, newTextLineHeight)
    newTextLineHeight += textHeight

    console.log("newTextLineHeight", newTextLineHeight)
    //mini code size
    let miniCodeHeight = 60
    let miniCodeWidth = 60
    // console.log("newHeight", newHeight)
    // console.log("height", height)
    // console.log("height - miniCodeHeight / 2", height - miniCodeHeight / 2)
    //draw mini code
    ctx.drawImage(miniCodeImg, width / 2 - miniCodeWidth / 2, height - miniCodeHeight - 0.05 * miniCodeHeight, miniCodeWidth, miniCodeHeight)

    ctx.draw()
  }

})